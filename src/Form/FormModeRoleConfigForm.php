<?php

/**
 * @author romsP
 * @file
 * Contains \Drupal\form_mode_role\Form\FormModeRoleConfigForm.
 */

namespace Drupal\form_mode_role\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\Entity\Role;

class FormModeRoleConfigForm extends ConfigFormBase{

    protected $configFactory;

    protected $entityTypeManager;

    protected $storage;

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('entity_type.manager')
        );
    }
    /**
     * Constructs a \Drupal\system\ConfigFormBase object.
     *
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     *   The factory for configuration objects.
     */
    public function __construct(EntityTypeManagerInterface $entity_manager) {
        $this->entityTypeManager = $entity_manager;
        $this->storage = $entity_manager->getStorage('entity_form_display');
        $this->configFactory = parent::configFactory();
    }



    /**
     * Returns a unique string identifying the form.
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId()
    {
        return 'form_mode_role_config';
    }

    /**
     * Gets the configuration names that will be editable.
     *
     * @return array
     *   An array of configuration object names that are editable if called in
     *   conjunction with the trait's config() method.
     */
    protected function getEditableConfigNames()
    {
        return ['form_mode_role.configuration'];
    }

    /**
     * Form constructor.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, FormStateInterface $form_state){
        $config = $this->configFactory->get('form_mode_role.settings')->get('form_mode_role_admin');
        $role_names = array();
        $role_permissions = array();
        $admin_roles = array();
        foreach (Role::loadMultiple() as $role_name => $role) {
            // Retrieve role names for columns.
            $role_names[$role_name] = $role->label();
            // Fetch permissions for the roles.
            $role_permissions[$role_name] = $role->getPermissions();
            $admin_roles[$role_name] = $role->isAdmin();
        }
        // Get ready to store class for JS checkboxes uniques mode
        $jsRadio = array();
        // Store $role_names for use when saving the data.
        $form['role_names'] = array(
            '#type' => 'value',
            '#value' => $role_names,
        );
        $form['form_mode'] = array(
            '#type' => 'table',
            '#header' => array($this->t('Form mode')),
            '#id' => 'form_mode',
            '#attributes' => ['class' => ['permissions', 'js-permissions']],
            '#sticky' => TRUE,
        );
        foreach ($role_names as $name) {
            $form['form_mode']['#header'][] = array(
                'data' => $name,
                'class' => array('checkbox'),
            );
        }
        foreach ($this->getActivesFormMode() as $key => $entity_form_mode){
            //if(!isset($form['form_mode'][$entity_form_mode["entity_type"]])) {
               $form['form_mode'][$entity_form_mode["entity_type"]] = array(array(
                    '#wrapper_attributes' => array(
                        'colspan' => count($role_names) + 1,
                        'class' => array('form_mode_role_entity'),
                        'id' => 'module-' . $entity_form_mode['entity_type'],
                    ),
                    '#markup' => strtoupper($entity_form_mode['entity_type']),
                ));
            $form['form_mode'][$entity_form_mode["entity_type"].'.'.$entity_form_mode["entity_bundle"]] = array(array(
                '#wrapper_attributes' => array(
                    'colspan' => count($role_names) + 1,
                    'class' => array('form_mode_role_bundle'),
                    'id' => 'module-' . $entity_form_mode['entity_bundle'],
                ),
                '#markup' => $entity_form_mode['entity_bundle'],
            ));
            $form['form_mode'][$entity_form_mode["entity_type"].'.'.$entity_form_mode["entity_bundle"].'.'.$entity_form_mode["entity_mode"].'.edit']['description'] = array(
              '#type' => 'link',
              '#title' => $entity_form_mode["entity_mode"].' on EDIT',
              '#url' => Url::fromRoute('entity.entity_form_display.'.$entity_form_mode["entity_type"].'.form_mode',[
                'node_type' => $entity_form_mode["entity_bundle"],
                'form_mode_name' => $entity_form_mode["entity_mode"]
              ]),
              '#attributes' => [
                'class' => ['use-ajax'],
                'data-dialog-type' => 'modal',
                'data-dialog-options' => Json::encode(['width' => "50%", 'height' => "70%"])
              ],
              '#wrapper_attributes' => array(
                'class' => array('form_mode_role_edit'),
              ),
            );
            $form['form_mode'][$entity_form_mode["entity_type"].'.'.$entity_form_mode["entity_bundle"].'.'.$entity_form_mode["entity_mode"].'.default']['description'] = array(
                '#type' => 'inline_template',
                '#template' => '<div class="permission"><span class="title">'.$entity_form_mode["entity_mode"].' on ADD'.'</span></div>',
                '#context' => array(
                    'title' => $key,
                ),
                '#wrapper_attributes' => array(
                    'class' => array('form_mode_role_add'),
                ),
            );
            foreach ($role_names as $rid => $name) {
                $role_current = Role::load($rid);
                $form['form_mode'][$entity_form_mode["entity_type"].'.'.$entity_form_mode["entity_bundle"].'.'.$entity_form_mode["entity_mode"].'.edit'][$rid] = array(
                    '#title' => $name . ': ' . $entity_form_mode['entity_mode'],
                    '#title_display' => 'invisible',
                    '#wrapper_attributes' => array(
                        'class' => array('checkbox','form_mode_role_edit'),
                    ),
                    '#attributes' => array('class' => ($role_current->hasPermission("form_mode_role_".$entity_form_mode["entity_type"]."_".$entity_form_mode["entity_bundle"]."_".$entity_form_mode["entity_mode"]))?array('form_mode_role_disabled',
                        $entity_form_mode["entity_type"].'_'.$entity_form_mode["entity_bundle"].'_'.$rid.'_edit'):array('form_mode_role_enabled',
                        $entity_form_mode["entity_type"].'_'.$entity_form_mode["entity_bundle"].'_'.$rid.'_edit'),
                    ),
                    '#disabled' => ($role_current->hasPermission("form_mode_role_".$entity_form_mode["entity_type"]."_".$entity_form_mode["entity_bundle"]."_".$entity_form_mode["entity_mode"]) || $role_current->hasPermission('form_mode_role_access_all'))?FALSE:TRUE,
                    '#type' => 'checkbox',
                    '#default_value' => isset($config[$entity_form_mode["entity_type"]][$entity_form_mode["entity_bundle"]][$entity_form_mode["entity_mode"]]['edit'][$rid]) ? 1 : 0,
                );
                $form['form_mode'][$entity_form_mode["entity_type"].'.'.$entity_form_mode["entity_bundle"].'.'.$entity_form_mode["entity_mode"].'.default'][$rid] = array(
                    '#title' => $name . ': ' . $entity_form_mode['entity_mode'],
                    '#title_display' => 'invisible',
                    '#wrapper_attributes' => array(
                        'class' => array('checkbox','form_mode_role_add'),
                    ),
                    '#attributes' => array('class' => ($role_current->hasPermission("form_mode_role_".$entity_form_mode["entity_type"]."_".$entity_form_mode["entity_bundle"]."_".$entity_form_mode["entity_mode"]))?array('form_mode_role_disabled',
                        $entity_form_mode["entity_type"].'_'.$entity_form_mode["entity_bundle"].'_'.$rid.'_default'):array('form_mode_role_enabled',
                        $entity_form_mode["entity_type"].'_'.$entity_form_mode["entity_bundle"].'_'.$rid.'_default')
                    ),
                    '#disabled' => ($role_current->hasPermission("form_mode_role_".$entity_form_mode["entity_type"]."_".$entity_form_mode["entity_bundle"]."_".$entity_form_mode["entity_mode"]) || $role_current->hasPermission('form_mode_role_access_all'))?FALSE:TRUE,
                    '#type' => 'checkbox',
                    '#default_value' => isset($config[$entity_form_mode["entity_type"]][$entity_form_mode["entity_bundle"]][$entity_form_mode["entity_mode"]]['default'][$rid]) ? 1 : 0,
              );
                $jsRadio[] = $entity_form_mode["entity_type"].'_'.$entity_form_mode["entity_bundle"].'_'.$rid.'_edit';
                $jsRadio[] = $entity_form_mode["entity_type"].'_'.$entity_form_mode["entity_bundle"].'_'.$rid.'_default';
            }
        }

        $form['#attached']['library'][] = 'form_mode_role/form_mode_role';
        $form['#attached']['drupalSettings']['form_mode_role'] = $jsRadio;
        $form = parent::buildForm($form, $form_state);
        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $form_mode_config = array();
        $configuration = $this->configFactory->getEditable('form_mode_role.settings');
        foreach ($form_state->getValue('form_mode') as $form_mode => $option) {
            $entity_form_mode = explode(".", $form_mode);
            if(count($entity_form_mode) == 4) {
                foreach ($option as $role => $enabled){
                    if($enabled != 0) {
                        $form_mode_config[$entity_form_mode[0]][$entity_form_mode[1]][$role][$entity_form_mode[3]] = implode('.',[$entity_form_mode[0],$entity_form_mode[1],$entity_form_mode[2]]);
                        $form_mode_config_admin[$entity_form_mode[0]][$entity_form_mode[1]][$entity_form_mode[2]][$entity_form_mode[3]][$role] = $enabled;
                    }
                }
            }
        }
        $configuration->set('form_mode_role', $form_mode_config);
        $configuration->set('form_mode_role_admin',$form_mode_config_admin);
        $configuration->save();

        parent::submitForm($form, $form_state);
    }


    /**
     * Get the list of entities with their form mode
     * @return array
     */
    protected function getEntitiesFormMode() {
        $entity_types = array();


        foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
            // See formModeTypeSelection() in core/modules/field_ui/src/Controller/EntityDisplayModeController.php
            if ($entity_type->get('field_ui_base_route') && $entity_type->hasFormClasses()) {
                $entity_types[0][$entity_type_id] = $entity_type->getLabel();
            }
            if ($entity_type->getBaseTable() != NULL && $entity_type->hasFormClasses()) {
                //Save content entities which have form modes.
                $entity_types[1][$entity_type_id] = $entity_type->getLabel();
            }
        }
        return $entity_types;
    }

    /**
     * Get the list of actives forms modes
     *
     * @return array
     */
    protected function getActivesFormMode() {
        $entites_form_mode = array();
        $form_modes = $this->storage->loadMultiple();
        //@TODO populate default and remove unique form mode a smart way
        $only_foo = array();
        foreach ($form_modes as $id_form_mode => $form_mode) {
            if(isset($only_foo[$form_mode->getTargetEntityTypeId()][$form_mode->getTargetBundle()])) {
                $only_foo[$form_mode->getTargetEntityTypeId()][$form_mode->getTargetBundle()] += 1;
            }else {

                $only_foo[$form_mode->getTargetEntityTypeId()][$form_mode->getTargetBundle()] = 1;
            }
        }
        foreach ($form_modes as $id_form_mode => $form_mode) {
            if($form_mode->status() && $only_foo[$form_mode->getTargetEntityTypeId()][$form_mode->getTargetBundle()] > 1) {
                $entites_form_mode[$id_form_mode]['entity_type'] = $form_mode->getTargetEntityTypeId();
                $entites_form_mode[$id_form_mode]['entity_bundle'] = $form_mode->getTargetBundle() ;
                $entites_form_mode[$id_form_mode]['entity_mode'] = $form_mode->getMode() ;
            }
        }

        return $entites_form_mode;
    }
}