<?php

namespace Drupal\form_mode_role;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class FormModeRolePermission Implements ContainerInjectionInterface  {
    use StringTranslationTrait;
    /**
     * The entity type manager.
     *
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityManager;

    protected $configFactory;

    public static function create(ContainerInterface $container)
    {
        return new static(
        $container->get('entity_type.manager'),
        $container->get('config.factory'),
        $container->get('string_translation')
    );
    }

    /**
     * Constructs a new FormModePermission instance.
     *
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
     *   The entity manager.
     */
    public function __construct(EntityTypeManagerInterface $entity_manager, ConfigFactory $configFactory, TranslationInterface $translation) {
        $this->entityManager = $entity_manager;
        $this->configFactory = $configFactory;
        $this->stringTranslation = $translation;
    }

    /**
     * @return array
     */
    public function addPermissions()
    {
        $permissions = [];
        $form_modes = $this->entityManager->getStorage('entity_form_display')->loadMultiple();
        foreach ($form_modes as $id_form_mode => $form_mode) {
            $only_foo = array();
            foreach ($form_modes as $key => $value) {
                if (preg_match('/^'.$form_mode->getTargetEntityTypeId().'./', $key) === 1) {
                    if($value->getMode() != 'default')
                    $only_foo[$key] = 1;
                }
            }

            if ($form_mode->status() && count($only_foo) > 0) {
                $permissions["form_mode_role_".$form_mode->getTargetEntityTypeId()."_".$form_mode->getTargetBundle()."_".$form_mode->getMode()] = [
                    'title' => $this->t('%entity_type_id: %bundle enable %label_form_mode .', array(
                        '%label_form_mode' => $form_mode->getMode(),
                        '%entity_type_id' => $form_mode->getTargetEntityTypeId(),
                        '%bundle' => $form_mode->getTargetBundle())),
                ];
            }
        }
        $permissions['form_mode_role_access_all'] = [
            'title' => $this->t('Access all form mode'),
            'description' => $this->t('Enable for all the entity. This permission bypass the others.'),
        ];

        // See sortPermissions() dans  core/modules/user/src/PermissionHandler.php pour la facon de sort l array de perms
        return $permissions;
    }
}