(function ($, Drupal, drupalSettings) {

    $(document).ready(function () {
        $.each(drupalSettings.form_mode_role,function(i,v) {
            $('.'+v).on('change', function() {
                $('.'+v).not(this).prop('checked', false);
            });
        });
    });
})(jQuery, Drupal, drupalSettings);